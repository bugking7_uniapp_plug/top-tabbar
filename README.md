# 顶部导航栏
## 简介

顶部导航栏是**可以横向滑动**的自定义导航栏。

## 属性

属性|类型|必填|备注
-|-|-|-
tabItem|Array| 是| 导航栏数据，默认为空，详情见`tabItem`
options |Object | 是| 导航栏样式参数，详情见`options`

#### 1、tabItem

属性 | 类型 | 必填 | 备注
-|-|-|-
name | String | 是 | 导航栏单元标题
id | Number | 是 | id

#### 2、options

属性 | 类型 | 必填 | 备注
-|-|-|-
top_backgroundcolor | String | 是 | 导航栏背景颜色值，默认为'#e64d3a'
top_itemcolor | String | 是 | 导航栏单元标题颜色值，默认为'#000000'
top_itemselectcolor | String | 是 | 导航栏单元标题选中时颜色值，默认为'#ffffff'

## 事件

事件名| 备注
-|-
@tabItemClick | 导航栏单元标题点击回调，返回值event:{index:id}，返回值为，event.index，注：此处的index为当前选中的`tabItem`的id，并非下标index

## 基本用法
    <topbar @tabItemClick="handleItemClick" :options="options" :tabItem="tabItem"></topbar>


	import topbar from '../../components/top-tabbar/top-tabbar.vue'
    data:{
        return {
			options:{
				top_backgroundcolor:'#e64d3a',
				top_itemcolor:'#000000',
				top_itemselectcolor:'#ffffff'
			},
			tabItem: [
				{
					name: '中国明星',
					id: 2001
				},
				{
					name: '欧美明星',
					id: 2002
				}
			]
		}
    }

    methods: {
		handleItemClick(e) {
			console.log(e)
		}
	}

## 兼容性

微信、QQ小程序、Android、Edge实测没问题，IOS没有本子尚未测试。


## 其他

遇到问题欢迎评论指出，感谢你的关注和支持。


【蝴蝶壁纸】QQ小程序，每天分享免费壁纸！

![avatar](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-c4651ffd-8264-432b-af24-d142b3731b81/b78c6983-d943-460d-845d-906e98df5740.png)

【缘来是星座】微信小程序，每天1点、7点、17点更新十二星座运势，还可以查看你的他（她）和你是否配对奥！

![avatar](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-c4651ffd-8264-432b-af24-d142b3731b81/44fa0025-8e93-4aed-80e1-38fbf48efd5b.jpg)
